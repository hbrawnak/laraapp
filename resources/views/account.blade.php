@extends('layouts.master')

@section('title')
    Account
@endsection

@section('content')
 <section class="row new-post">
    <div class="col-md-6 col-md-offset-3">
        <header><h3>Your Account</h3></header>
        <form method="post" action="{{ route('account.save') }}" enctype="multipart/form-data">
        <div class="form-group">
            <label for="first_name">Name:</label>
            <input class="form-control" type="text" name="first_name" id="first_name" value="{{ Auth::user()->first_name }}"/>
        </div>
        <div class="form-group">
            <label for="email">Email:</label>
            <p class="form-control">{{ Auth::user()->email }}</p>
        </div>
        <div class="form-group">
            <label for="profile_image">Profile Image:</label>
            <input name="image" id="image" class="form-control" type="file">
        </div>
        <input class="btn btn-default" type="submit" value="Submit">
        <input type="hidden" name="_token" value="{{ Session::token() }}">
        </form>
    </div>
 </section>
@if (Storage::disk('local')->has(Auth::user()->first_name . '-' . Auth::user()->id . '.jpg'))
    <section class="row new-post">
        <div class="col-md-6 col-md-offset-3 image-size">
            <label for="image">Profile Image:</label>
            <img src="{{ route('account.image', ['filename' => Auth::user()->first_name . '-' . Auth::user()->id . '.jpg']) }}" alt="" class="img-responsive">
        </div>
    </section>
@endif
@endsection