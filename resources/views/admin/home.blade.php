@extends('admin.layouts.master')

@section('title')
    Admin | Home
@endsection

@section('admin_content')

<div class="table-responsive">
  <table class="table table-bordered">
    <thead>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
        <th>User</th>
        <th>Author</th>
        <th>Admin</th>
    </thead>
    <tbody>
        <tr>
            <td>Habib</td>
            <td>Rahman</td>
            <td>hbrawnak@gmail.com</td>
            <td>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" value="">
                    User
                  </label>
                </div>
             </td>
            <td>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" value="">
                    Author
                  </label>
                </div>
            </td>

           <td>
               <div class="checkbox">
                 <label>
                   <input type="checkbox" value="">
                   Admin
                 </label>
               </div>
           </td>
          </tr>
    </tbody>
  </table>
</div>
@endsection